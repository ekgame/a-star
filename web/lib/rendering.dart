import 'dart:html';
import 'grid.dart';
import 'dart:math';
import 'maze_generator.dart';
import 'pathfinding.dart';

class Cell {
  bool solid = false;
  bool marker = false;
  int x, y;
  Cell(this.x, this.y);
}

class Scene {

  CanvasElement canvas;
  Grid<Cell> grid;
  int cellSize;

  num _mouseX = 0, _mouseY = 0;
  bool editing = false;

  bool drawing = false;
  bool removing = false;
  bool canCutCorners = false;

  Cell markerStart, markerEnd;
  List<Point> path;
  GridPF pathFinder;

  Function heuristic = Heuristic.octile;
  int display = 0;

  Scene(CanvasElement this.canvas, int this.cellSize) {

    int gridWidth = canvas.width ~/ cellSize;
    int gridHeight = canvas.height ~/ cellSize;

    grid = new Grid<Cell>(gridWidth, gridHeight, (x, y) => new Cell(x, y));

    canvas.onMouseMove.capture((e){
      e.preventDefault();
      var clientRect = canvas.getBoundingClientRect();
      _mouseX = e.client.x - clientRect.left;
      _mouseY = e.client.y - clientRect.top;
    });

    canvas.onMouseDown.capture((e) => onMouseClick(e.button, true));
    canvas.onMouseUp.capture((e) => onMouseClick(e.button, false));
  }

  void onMouseClick(int button, bool down) {
    Cell current = grid[_mouseX~/cellSize][_mouseY~/cellSize];

    if (button == 0 && down && editing) {
      if (current.solid) removing = true; else drawing = true;
    } else {
      drawing = false;
      removing = false;
    }

    if (button == 0 && down && !editing) {
      if (current.solid) return;

      if (markerStart == null && markerEnd == null) {
        markerStart = current;
        markerStart.marker = true;
      } else if (markerStart != null && markerEnd != null) {
        markerStart.marker = false;
        markerEnd.marker = false;
        markerStart = current;
        markerStart.marker = true;
        markerEnd = null;
      } else {
        markerEnd = current;
        markerEnd.marker = true;
        doPathFinding();
      }
    }
  }

  void doPathFinding() {
    pathFinder = new GridPF(grid.width, grid.height, (x,y) => grid[x][y].solid, heuristic);
    pathFinder.setStart(markerStart.x, markerStart.y);
    pathFinder.setEnd(markerEnd.x, markerEnd.y);
    pathFinder.setAllowCutCorners(canCutCorners);
    path = pathFinder.getPath();
  }

  void setAllowCutCorners(bool allow) {
    canCutCorners = allow;
    if (markerStart != null && markerEnd != null)
      doPathFinding();
  }

  void generateMaze() {
    clear();
    GridMaze maze = new GridMaze(grid.width, grid.height);
    maze.generateMaze();
    grid.forEach((x, y, cell){
      cell.solid = maze[x][y].wall;
      cell.marker = false;
      path = null;
    });
  }

  void clear() {
    grid.forEach((x, y, cell){
      cell.solid = false;
      cell.marker = false;
      path = null;
    });
    markerStart = null;
    markerEnd = null;
    pathFinder = null;
  }

  void render(double delta) {

    Cell current = grid[_mouseX~/cellSize][_mouseY~/cellSize];

    bool old = current.solid;
    if (drawing)
     current.solid = true;
    if (removing)
      current.solid = false;

    if (old != current.solid && markerStart != null && markerEnd != null)
      doPathFinding();

    CanvasRenderingContext2D context = canvas.context2D;
    context
      ..fillStyle = "white"
      ..fillRect(0, 0, canvas.width, canvas.height)
      ..fillStyle = "blue"
      ..fillRect(_mouseX, _mouseY, 2, 2)
      ..font = "7px Arial";

    for (int x = 0; x < grid.width; x++) {
      for (int y = 0; y < grid.height; y++) {
        Cell c = grid[x][y];
        String color = "white";
        if (pathFinder != null && pathFinder[x][y].wasOpen) color = "#91FFB4"; // light green
        if (pathFinder != null && pathFinder[x][y].wasClosed) color = "#FF9191"; // light red
        if (c.solid) color = "black";
        if (c.marker) color = "blue";

        context
          ..fillStyle =  color
          ..fillRect(x*cellSize, y*cellSize, cellSize, cellSize);

        if (pathFinder != null && !c.solid && display != 0) {
          String text = "${pathFinder[x][y].hCost}";
          if (display == 2) text = "${pathFinder[x][y].gCost}";
          if (display == 3) text = "${pathFinder[x][y].fCost}";
          context
            ..fillStyle = "black"
            ..fillText(text, x * cellSize + 2, y * cellSize + 10);
        }
      }
    }

    context.lineWidth = 1;

    for (int x = 0; x < grid.width; x++) {
      context
        ..strokeStyle = "gray"
        ..beginPath()
        ..moveTo(x*cellSize, 0)
        ..lineTo(x*cellSize, canvas.height)
        ..closePath()
        ..stroke();
    }

    for (int y = 0; y < grid.height; y++) {
      context
        ..strokeStyle = "gray"
        ..beginPath()
        ..moveTo(0, y*cellSize)
        ..lineTo(canvas.width, y*cellSize)
        ..closePath()
        ..stroke();
    }

    if (path != null) {
      context
        ..lineWidth = 5
        ..strokeStyle = "green"
        ..beginPath()
        ..moveTo(path[0].x*cellSize + cellSize/2, path[0].y*cellSize + cellSize/2);

      for (int i = 1; i < path.length; i++)
        context.lineTo(path[i].x*cellSize + cellSize/2, path[i].y*cellSize + cellSize/2);

      context.stroke();
    }
  }

  void setHeuristic(int value) {
    switch (value) {
      case 0: heuristic = Heuristic.octile; break;
      case 1: heuristic = Heuristic.euclidean; break;
      case 2: heuristic = Heuristic.manhattan; break;
      case 3: heuristic = Heuristic.chebyshev; break;
      case 4: heuristic = Heuristic.dijkstra; break;
    }
    if (markerStart != null && markerEnd != null)
      doPathFinding();
    print("sfsdfsd");
  }

  void setDisplay(int display) {
    this.display = display;
  }
}