import 'grid.dart';
import 'dart:math';

class MazeCell {
  bool wall = true;
  int dir = -1; // -1 = not explored,  0 = x+,  1 = x-,  2 = y+,  3 = y-
}

class GridMaze extends Grid<MazeCell> {
  GridMaze(int width, int height) : super(width, height, (x,y) => new MazeCell());

  static const int startX = 1, startY = 1;
  int posX = startX, posY = startY;

  void generateMaze() {
    this[posX][posY].wall = false;
    this[posX][posY].wall = false;
    this[posX][posY].dir = -2;

    Random rng = new Random();
    while (true) {
      List<int> directions = validDirections();
      if (directions.isEmpty) {
        int dir = this[posX][posY].dir;
        if (dir == -2) break;
        move(dir, false);
      } else {
        int dir = directions[rng.nextInt(directions.length)];
        move(dir, true);
      }
    }
  }

  List<int> validDirections() {
    List<int> list = [];

    if (posX+2 < width && this[posX+2][posY].wall) list.add(0);
    if (posX-2 >= 0 && this[posX-2][posY].wall) list.add(1);
    if (posY+2 < height && this[posX][posY+2].wall) list.add(2);
    if (posY-2 >= 0 && this[posX][posY-2].wall) list.add(3);

    return list;
  }

  void move(int dir, bool changeDir) {
    if (dir == 0) {
      if (changeDir) {
        this[posX + 1][posY].wall = false;
        this[posX + 2][posY].wall = false;
        this[posX + 2][posY].dir = opposite(dir);
      }
      posX += 2;
    } else if (dir == 1) {
      if (changeDir) {
        this[posX - 1][posY].wall = false;
        this[posX - 2][posY].wall = false;
        this[posX - 2][posY].dir = opposite(dir);
      }
      posX -= 2;
    } else if (dir == 2) {
      if (changeDir) {
        this[posX][posY + 1].wall = false;
        this[posX][posY + 2].wall = false;
        this[posX][posY + 2].dir = opposite(dir);
      }
      posY += 2;
    } else if (dir == 3) {
      if (changeDir) {
        this[posX][posY - 1].wall = false;
        this[posX][posY - 2].wall = false;
        this[posX][posY - 2].dir = opposite(dir);
      }
      posY -= 2;
    }
  }

  int opposite(int dir) {
    if (dir == 0) return 1;
    if (dir == 1) return 0;
    if (dir == 2) return 3;
    if (dir == 3) return 2;
    return -1;
  }
}
