library pathfinding;

import 'grid.dart';
import 'dart:math';

class Heuristic {
  static num _sqr(num x) => x*x;
  static num _abs(num x) => x < 0 ? -x : x;

  static int euclidean(int x1, int y1, int x2, int y2) {
    return (sqrt(_sqr(x2 - x1) + _sqr(y2 - y1)) * 10).round();
  }

  static int chebyshev(int x1, int y1, int x2, int y2) {
    return max(_abs(x1 - x2), _abs(y1 - y2));
  }

  static int octile(int x1, int y1, int x2, int y2) {
    int dx = _abs(x1 - x2);
    int dy = _abs(y1 - y2);
    return 10*(dx + dy) - 6*min(dx, dy);
  }

  static int manhattan(int x1, int y1, int x2, int y2) {
    return _abs(x1 - x2) + _abs(y1 - y2);
  }

  static int dijkstra(int x1, int y1, int x2, int y2) {
    return 0;
  }
}

class Cell {
  bool wall = false;
  int gCost = 0;
  int hCost;
  int x, y;
  Cell parent;

  bool wasOpen = false;
  bool wasClosed = false;

  Cell(this.x, this.y, this.wall);

  int get fCost => gCost + hCost;

  bool operator == (Cell other) => x == other.x && y == other.y;
}

class GridPF extends Grid<Cell> {

  Cell startPos, endPos;
  bool canCutCorners = false;
  Function heuristic;

  GridPF(int width, int height, bool isWall(int x, int y), [int this.heuristic(int x1, int y1, int x2, int y2) = Heuristic.octile]) :
  super(width, height, (x, y) => new Cell(x, y, isWall(x, y)));

  setStart(int x, int y) => startPos = this[x][y];
  setEnd(int x, int y) => endPos = this[x][y];
  setAllowCutCorners(bool allow) => canCutCorners = allow;

  List<Point> getPath() {
    if (startPos == null || endPos == null)
      return null;

    num sqr(x) => x*x;

    this.forEach((x, y, cell) {
      cell.hCost = heuristic(x, y, endPos.x, endPos.y);
    });

    List<Cell> openCells = [startPos];
    List<Cell> closedCells = [];

    List<Point> backtrack(Cell ending) {
      List<Point> points = [];
      Cell current = ending;
      while (current != null) {
        points.add(new Point(current.x, current.y));
        current = current.parent;
      }
      return points.reversed.toList();
    }

    bool canStep(Cell from, Cell to) {
      if (from.x == to.x || from.y == to.y)
        return true;

      if (to.y < from.y && this[from.x][from.y-1].wall)
        return false;
      if (to.y > from.y && this[from.x][from.y+1].wall)
        return false;

      if (to.x < from.x && this[from.x-1][from.y].wall)
        return false;
      if (to.x > from.x && this[from.x+1][from.y].wall)
        return false;

      return true;
    }

    while (!openCells.isEmpty) {
      Cell current;
      openCells.forEach((cell) {
        if (current == null || cell.fCost < current.fCost)
          current = cell;
      });

      if (current == endPos)
        return backtrack(current);

      int addX = (current.x - 1) < 0 ? 1 : (current.x + 1) > width ? -2 : 0;
      int addY = (current.y - 1) < 0 ? 1 : (current.y + 1) > height ? -2 : 0;

      Grid<Cell> neighbours = this.subGrid(current.x - 1, current.y - 1, 3, 3);
      neighbours.forEach((x, y, e) {
        if (e == current || e.wasClosed || e.wall)
          return;

        if (!canCutCorners && !canStep(current, e))
          return;

        int newGCost = current.gCost + 10 + (((x+y+addX+addY) % 2) == 0 ? 4 : 0);
        if (e.wasOpen && newGCost < e.gCost || e.gCost == 0) {
          e.gCost = newGCost;
          e.parent = current;
        }
        if (!e.wasOpen) {
          openCells.add(e);
          e.wasOpen = true;
        }
      });

      closedCells.add(current);
      openCells.remove(current);
      current.wasClosed = true;
    }

    return null;
  }
}