library grid;

class Grid<T> {
  List<List<T>> items = [];
  List<_Column<T>> _columns = [];
  int _width, _height;

  Grid(int this._width, int this._height, [T fill(int x, int y)]) {
    for (int i = 0; i < _height; i++) {
      if (fill == null)
        items.add(new List.filled(_width, null));
      else {
        List<T> row = [];
        for (int o = 0; o < _width; o++)
          row.add(fill(o, i));
        items.add(row);
      }
    }

    for (int i = 0; i < _width; i++)
      _columns.add(new _Column<T>(this, i));
  }

  void forEach(void accept(int x, int y, T cell)) {
    for (int x = 0; x < _width; x++)
      for (int y = 0; y < _height; y++)
        accept(x, y, this[x][y]);
  }

  _Column<T> operator [](int x) => _columns[x];

  int get width => _width;
  int get height => _height;

  List<T> getColumn(int x) {
    List<T> list = [];
    for (int i = 0; i < height; i++)
      list.add(items[i][x]);
    return list;
  }

  List<T> getRow(int y) {
    List<T> list = [];
    for (int i = 0; i < width; i++)
      list.add(items[y][i]);
    return list;
  }

  Grid<T> subGrid(int x, int y, int width, int height) {
    if (x < 0) {width += x; x = 0;}
    if (y < 0) {height += y; y = 0;}

    if (x + width > this.width) {width = this.width - x;}
    if (y + height > this.height) {height = this.height - y;}

    Grid<T> grid = new Grid(width, height);
    for (int xx = x; xx < x + width; xx++)
      for (int yy = y; yy < y + height; yy++)
        grid[xx - x][yy - y] = this[xx][yy];

    return grid;
  }
}

class _Column<T> {
  Grid _grid;
  int _column;
  _Column(Grid this._grid, int this._column);

  T operator [](int y) => _grid.items[y][_column];
  operator []=(int y, T value) => _grid.items[y][_column] = value;
}

