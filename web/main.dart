// Copyright (c) 2015, <your name>. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

import 'dart:html';
import 'lib/rendering.dart';

Scene scene;

void main() {
  scene = new Scene(querySelector('#output'), 20);

  querySelector('#clear').onClick.listen((e){
    scene.clear();
  });

  querySelector('#maze').onClick.listen((e){
    scene.generateMaze();
  });

  bool editing = false;
  querySelector('#toggle').onClick.listen((e){
    editing = !editing;
    InputElement el = querySelector('#toggle');
    el.value = editing ? "Editing" : "Path finding";
    scene.editing = editing;
  });

  querySelector('#heuristic').onChange.listen((e) {
    SelectElement el = querySelector('#heuristic');
    scene.setHeuristic(int.parse(el.value));
  });

  querySelector('#display').onChange.listen((e) {
    SelectElement el = querySelector('#display');
    scene.setDisplay(int.parse(el.value));
  });

  querySelector('#allowCorners').onClick.listen((e){
    InputElement el = querySelector('#allowCorners');
    scene.setAllowCutCorners(el.checked);
  });

  window.requestAnimationFrame(draw);
}

num prevTime = 0;

void draw(num time)
{
  double delta = (time - prevTime)/1000;
  prevTime = time;
  scene.render(delta);
  window.requestAnimationFrame(draw);
}